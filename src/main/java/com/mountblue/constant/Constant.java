package com.mountblue.constant;

public class Constant {

    public static final Integer LIMIT = 10;

    public static final String SPACE = " ";
    public static final String ASC = "asc";
    public static final String DESC = "desc";

    public static final String PUBLISH = "publish";
    public static final String SAVE = "save";
    public static final String PUBLISHED_AT = "publishedAt";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";

    public static final String TEXT_PLAIN = "text/plain";
    public static final String MAIL_SEND = "mail/send";

    public static final String OK = "Ok";
    public static final String SUCCESS = "Success";
    public static final String LINK_SEND = "Conformation link send to your e-mail account";
    public static final String ACCOUNT_VERIFIED = "Account verified";
    public static final String PASSWORD_CHANGED = "Password Changed Successfully";
    public static final String FAILED = "Failed";

    public static final String VERIFY_ACCOUNT_MESSAGE = "To conform this account please click here : ";
    public static final String VERIFY_ACCOUNT_LINK = "http://ec2-13-235-75-254.ap-south-1.compute.amazonaws.com/conform-account/token/";
    public static final String VERIFY_ACCOUNT_SUBJECT = "Verify Account";
    public static final String FORGOT_PASSWORD_MESSAGE = "To change password please click here : ";
    public static final String FORGOT_PASSWORD_LINK = "http://ec2-13-235-75-254.ap-south-1.compute.amazonaws.com/change-password/token/";
    public static final String FORGOT_PASSWORD_SUBJECT = "Change Password";

    public static final String EMAIL_REGEX = "[a-zA-Z0-9][a-zA-Z0-9]*[._]?[a-zA-Z0-9]+@[a-zA-Z]+[.][a-zA-Z]+";

}
