package com.mountblue.constant;

public class Error {

    public static final String NO_BLOG_FOUND = "No Blogs Found";
    public static final String BLOG_NOT_FOUND = "Blog Not Found";
    public static final String PAGE_NOT_ACCEPTABLE = "Page Number Not Acceptable";
    public static final String NO_RECORD_FOUND = "No Records Found";
    public static final String BAD_REQUEST = "Bad Request";
    public static final String INVALID_COMMENT = "Invalid Comment";
    public static final String COMMENT_NOT_FOUND = "Comment Not Found";
    public static final String UNAUTHORIZED_ACCESS = "Unauthorized Access";
    public static final String RESOURCE_NOT_FOUND = "Resource Not Found";
    public static final String BAD_VALUE_SAVE_DRAFT = "Bad Value For Save Draft";
    public static final String INVALID_DATA = "Invalid Data Send In Body";
    public static final String INVALID_CREDENTIAL = "Invalid User name and Password";
    public static final String EMAIL_DOES_NOT_EXIST = "Email doesn't exist";
    public static final String INVALID_TOKEN = "Invalid token";
    public static final String TOKEN_EXPIRED = "Token Expired";
    public static final String MIN_LENGTH = "Minimum Password Length 6";
    public static final String PASSWORD_SHOULD_MATCH = "Password Should Match";
    public static final String USER_EXIST = "User already exist!";
    public static final String EMAIL_EXIST = "Email already exist!";
    public static final String USER_NOT_FOUND = "User Not Found";

}
