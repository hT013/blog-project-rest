package com.mountblue.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity(name = "comments")
public class Comment extends Time {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String comment;
    String userName;
    String email;

    @JsonIgnore
    @Transient
    private int userId;

    @ManyToOne
    Blog blog;

}
