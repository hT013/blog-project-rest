package com.mountblue.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity(name = "tags")
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String name;


    @ManyToMany(mappedBy = "tagSet", fetch = FetchType.EAGER)
    Set<Blog> blogs;

}
