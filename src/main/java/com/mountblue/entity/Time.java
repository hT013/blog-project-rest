package com.mountblue.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Data
@MappedSuperclass
public class Time {

    @Column(updatable = false)
    @CreationTimestamp
    Date createdAt;

    Date updatedAt;

}
