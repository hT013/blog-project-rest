package com.mountblue.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

}
