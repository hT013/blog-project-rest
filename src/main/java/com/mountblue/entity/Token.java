package com.mountblue.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int tokenId;
    private String token;
    private Date expiryDate;

    @OneToOne(fetch = FetchType.EAGER)
    User user;

}
