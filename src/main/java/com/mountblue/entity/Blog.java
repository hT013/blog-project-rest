package com.mountblue.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity(name = "blogs")
public class Blog extends Time {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int authorId;
    private String blogTitle;
    private String blogDescription;
    private String blogContent;
    private Date publishedAt;
    private boolean saveDraft;

    @Transient
    private String authorName;

    @JsonIgnoreProperties("blogs")
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "blogs_tags",
            joinColumns = @JoinColumn(name = "blog_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
    private Set<Tag> tagSet;

    @JsonIgnoreProperties("blog")
    @OneToMany(mappedBy = "blog", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Comment> commentList;

}
