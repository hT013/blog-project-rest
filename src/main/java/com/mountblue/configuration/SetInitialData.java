package com.mountblue.configuration;

import com.mountblue.entity.Role;
import com.mountblue.entity.User;
import com.mountblue.service.RoleService;
import com.mountblue.service.UserService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import static com.mountblue.constant.Constant.*;

@Component
public class SetInitialData {

    private final UserService userService;
    private final RoleService roleService;

    public SetInitialData(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    public void setData() {
        if (roleService.get(1).getId() == 0) {
            Role role = new Role();
            role.setName(ROLE_ADMIN);
            Role role1 = new Role();
            role1.setName(ROLE_USER);
            roleService.save(role);
            roleService.save(role1);
        }
        if (!userService.userExist("admin")) {
            User user = new User();
            user.setUserName("admin");
            user.setEmail("hitesh@ht.com");
            user.setIsEnabled(true);
            user.setRole(roleService.getByName(ROLE_ADMIN));
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            user.setPassword(bCryptPasswordEncoder.encode("admin@123"));
            userService.save(user);
        }
    }

}
