package com.mountblue.configuration;

import com.sendgrid.SendGrid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SendGridConfig {

    @Value("${sendgrid.api-key}")
    public String api;

    @Bean
    public SendGrid getSendGrid() {
        return new SendGrid(api);
    }

}
