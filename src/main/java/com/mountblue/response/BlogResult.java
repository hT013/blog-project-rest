package com.mountblue.response;

import com.mountblue.entity.Blog;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BlogResult {

    private int page;
    private int totalPage;
    private List<Blog> blogs;


}
