package com.mountblue.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class BlogData {

    @NotBlank
    @Size(min = 4)
    private String blogTitle;
    @NotBlank
    @Size(min = 4)
    private String blogDescription;
    @NotBlank
    @Size(min = 4)
    private String blogContent;
    @NotBlank
    @Size(min = 2)
    private String tags;
    @NotNull
    private boolean saveDraft;

}
