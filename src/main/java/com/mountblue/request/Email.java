package com.mountblue.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class Email {

    @NotBlank
    private String email;

}
