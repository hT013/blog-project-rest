package com.mountblue.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class Credential {

    @NotBlank
    private String userName;
    @NotBlank
    private String password;

}
