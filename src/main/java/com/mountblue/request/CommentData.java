package com.mountblue.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentData {

    private String comment;

}
