package com.mountblue.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class FormInput {

    @NotBlank
    @Size(min = 2)
    private String userName;

    @NotBlank
    @Size(min = 6)
    private String password;

    @NotBlank
    private String email;

    @NotBlank
    private String conformPassword;

}
