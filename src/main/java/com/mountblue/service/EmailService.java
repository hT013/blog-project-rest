package com.mountblue.service;

import com.mountblue.constant.Constant;
import com.mountblue.entity.Token;
import com.mountblue.event.SendEmailEvent;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.UUID;

import static com.mountblue.constant.Constant.*;


@Slf4j
@Service
@Component
public class EmailService {

    @Value("${sendgrid.email}")
    private String email;
    private final SendGrid sendGrid;
    private final TokenService tokenService;

    public EmailService(SendGrid sendGrid, TokenService tokenService) {
        this.sendGrid = sendGrid;
        this.tokenService = tokenService;
    }

    @EventListener
    public void mail(SendEmailEvent sendEmailEvent) {
        Token tokenObj = new Token();
        tokenObj.setUser(sendEmailEvent.getUser());
        String token = UUID.randomUUID().toString();
        Email from = new Email(email);
        Email to = new Email(tokenObj.getUser().getEmail());
        Content content = new Content(Constant.TEXT_PLAIN, sendEmailEvent.getMessage() + token);
        Mail mail = new Mail(from, sendEmailEvent.getSubject(), to, content);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint(MAIL_SEND);
            request.setBody(mail.build());
            Response response = sendGrid.api(request);
            if (response.getStatusCode() == 202) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.HOUR, 24);
                tokenObj.setExpiryDate(calendar.getTime());
                tokenObj.setToken(token);
                tokenService.save(tokenObj);
            } else {
                log.warn("Not able to send mail to user response : {}", response.getStatusCode());
            }
        } catch (Exception e) {
            log.error("Error in mail listener ", e);
        }
    }

}
