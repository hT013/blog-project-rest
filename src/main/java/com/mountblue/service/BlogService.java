package com.mountblue.service;

import com.mountblue.entity.Tag;
import com.mountblue.repository.BlogRepository;
import com.mountblue.entity.Blog;
import com.mountblue.request.BlogData;
import com.mountblue.response.BlogResult;
import com.mountblue.response.Status;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

import static com.mountblue.constant.Constant.*;
import static com.mountblue.constant.Error.*;

@Slf4j
@Service
public class BlogService {

    private final UserService userService;
    private final BlogRepository blogRepository;
    private final TagService tagService;

    public BlogService(BlogRepository blogRepository, UserService userService, TagService tagService) {
        this.blogRepository = blogRepository;
        this.userService = userService;
        this.tagService = tagService;
    }

    private void throwException(HttpStatus httpStatus, String message) {
        throw new ResponseStatusException(httpStatus, message);
    }

    private void saveBlogToDB(Set<Tag> tagSet, Blog blog) {
        blog.setUpdatedAt(new Date());
        blog.setTagSet(tagSet);
        save(blog);
    }

    private void checkAccess(int id, Authentication authentication) {
        if (userService.getUserDetails(authentication.getName()).getId() != id &&
                !authentication.getAuthorities().contains(new SimpleGrantedAuthority(ROLE_ADMIN))) {
            throwException(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_ACCESS);
        }
    }

    private BlogResult setAuthorName(List<Blog> blogs, int currentPage, int totalPages) {
        blogs.forEach(blog -> blog.setAuthorName(userService.get(blog.getAuthorId()).getUserName()));
        if (totalPages == 0 || blogs.size() == 0) {
            throwException(HttpStatus.BAD_REQUEST, NO_BLOG_FOUND);
        }
        if (++currentPage > totalPages) {
            throwException(HttpStatus.NOT_ACCEPTABLE, PAGE_NOT_ACCEPTABLE);
        }
        return new BlogResult(currentPage, totalPages, blogs);
    }

    private Integer getPage(Integer page) {
        if (page == null || page < 1) {
            log.warn("Invalid Page number : " + page);
            throwException(HttpStatus.NOT_ACCEPTABLE, PAGE_NOT_ACCEPTABLE);
        }
        return --page;
    }

    private void checkBlog(Blog blog, Integer id) {
        if (blog.getId() == 0) {
            log.warn("Blog Not Found [blog-id : " + id + "]");
            throwException(HttpStatus.NOT_FOUND, BLOG_NOT_FOUND);
        }
    }

    private Blog setBlogData(Blog blog, BlogData blogData) {
        blog.setBlogContent(blogData.getBlogContent());
        blog.setBlogDescription(blogData.getBlogDescription());
        blog.setBlogTitle(blogData.getBlogTitle());
        blog.setUpdatedAt(new Date());
        blog.setSaveDraft(blogData.isSaveDraft());
        return blog;
    }

    public void save(Blog blog) {
        blogRepository.save(blog);
    }

    public Blog get(Integer id) {
        Optional<Blog> blog = blogRepository.findById(id);
        return blog.orElseGet(Blog::new);
    }

    public Blog getBlog(Integer id) {
        Blog blog = blogRepository.findAllByIdAndSaveDraftFalse(id).orElseGet(Blog::new);
        blog.setAuthorName(userService.get(blog.getAuthorId()).getUserName());
        checkBlog(blog, id);
        return blog;
    }

    public Status delete(Integer id, Authentication authentication) {
        int authorId = get(id).getAuthorId();
        if (authorId == 0) {
            log.warn("Resolved[ Not Found : Blog not found]");
            return new Status(FAILED, BLOG_NOT_FOUND);
        }
        checkAccess(authorId, authentication);
        tagService.delete(get(id).getTagSet());
        blogRepository.deleteById(id);
        return new Status(OK, SUCCESS);
    }

    public Status update(Integer id, BlogData blogData, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors()) {
            throwException(HttpStatus.BAD_REQUEST, INVALID_DATA);
        }
        Blog blog1 = get(id);
        boolean isSaved = blog1.isSaveDraft();
        if (blog1.getId() == 0) {
            log.warn("Resolved[ Not Found : Blog not found]");
            throwException(HttpStatus.NOT_FOUND, BLOG_NOT_FOUND);
        }
        if (!blog1.isSaveDraft() && blogData.isSaveDraft()) {
            throwException(HttpStatus.BAD_REQUEST, INVALID_DATA);
        }
        checkAccess(blog1.getAuthorId(), authentication);
        Set<Tag> tagSet = tagService.save(blogData.getTags(), blog1);
        Blog blogObject = setBlogData(get(id), blogData);
        if (blog1.getPublishedAt() == null && isSaved && !blogData.isSaveDraft()) {
            blogObject.setPublishedAt(new Date());
        }
        saveBlogToDB(tagSet, blogObject);
        return new Status(OK, SUCCESS);
    }

    public Status publishOrSaveBlog(String operation, BlogData blogData, BindingResult bindingResult,
                                    Authentication authentication) {
        if (bindingResult.hasErrors()) {
            throwException(HttpStatus.BAD_REQUEST, INVALID_DATA);
        }
        Blog blog = setBlogData(new Blog(), blogData);
        switch (operation) {
            case PUBLISH:
                if (blogData.isSaveDraft()) {
                    throwException(HttpStatus.BAD_REQUEST, BAD_VALUE_SAVE_DRAFT);
                }
                blog.setPublishedAt(new Date());
                blog.setSaveDraft(false);
                break;
            case SAVE:
                if (!blogData.isSaveDraft()) {
                    throwException(HttpStatus.BAD_REQUEST, BAD_VALUE_SAVE_DRAFT);
                }
                blog.setSaveDraft(true);
                break;
            default:
                log.warn("Resolved[ Bad Request : Resource not found]");
                throwException(HttpStatus.BAD_REQUEST, RESOURCE_NOT_FOUND);
        }
        blog.setAuthorId(userService.getId(authentication.getName()));
        Set<Tag> tagSet = tagService.save(blogData.getTags(), blog);
        saveBlogToDB(tagSet, blog);
        return new Status(OK, SUCCESS);
    }

    public BlogResult getPublishedBlog(Integer id) {
        return setAuthorName(blogRepository.findBySaveDraftAndAuthorId(false, id), 0, 1);
    }

    public List<Blog> searchInBlog(String string, Integer id, Tag tag) {
        List<Blog> blogs;
        if (tag.getName() == null) {
            blogs = blogRepository.findByAuthorIdOrBlogTitleOrBlogDescriptionOrBlogContentOrderByPublishedAtDesc
                    (id, string, string, string);
        } else {
            blogs = blogRepository.findByAuthorIdOrTagSetOrBlogTitleOrBlogDescriptionOrBlogContentOrderByPublishedAtDesc
                    (id, tag, string, string, string);
        }
        List<Blog> blogList = new ArrayList<>();
        blogs.forEach(blog -> {
            if (!blog.isSaveDraft()) {
                blogList.add(blog);
            }
        });
        return blogList;
    }

    public BlogResult searchAuthorIdAndTag(Integer id, String field, Integer page, Integer limit, Tag tag) {
        page = getPage(page);
        if (tag.getId() == 0) {
            tag = null;
        }
        Page<Blog> blogPage = blogRepository.findBySaveDraftFalseAndAuthorIdAndTagSet(id, tag,
                PageRequest.of(page, limit, Sort.by(field).descending()));
        return setAuthorName(blogPage.toList(), page, blogPage.getTotalPages());
    }

    public BlogResult getBlogsInPage(Integer page, Integer pageSize, String sortBy) {
        page = getPage(page);
        PageRequest pageRequest = PageRequest.of(page, pageSize, Sort.by(sortBy).descending());
        Page<Blog> blogPage = blogRepository.findAllBySaveDraftFalse(pageRequest);
        return setAuthorName(blogPage.toList(), page, blogPage.getTotalPages());
    }

    public BlogResult getSortedByField(String field, String order, Integer page, Integer limit) {
        page = getPage(page);
        Page<Blog> blogPage;
        try {
            switch (order) {
                case DESC:
                    blogPage = blogRepository.findAllBySaveDraftFalse(PageRequest.of(page, limit,
                            Sort.by(field).descending()));
                    break;
                case ASC:
                    blogPage = blogRepository.findAllBySaveDraftFalse(PageRequest.of(page, limit,
                            Sort.by(field).ascending()));
                    break;
                default:
                    throw new Exception();
            }
        } catch (Exception e) {
            log.warn(e.getClass() + "Bad Request");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, BAD_REQUEST);
        }
        return setAuthorName(blogPage.toList(), page, blogPage.getTotalPages());
    }

    public BlogResult getPageData(String value, Integer page, Integer limit) {
        page = getPage(page);
        int index = page * LIMIT;
        Integer id = userService.getId(value);
        Tag tag = tagService.getTag(value);
        List<Blog> blogs = searchInBlog(value, id, tag);
        List<Blog> blogList = new ArrayList<>();
        while (index < blogs.size() && limit != 0) {
            blogList.add(blogs.get(index));
            index++;
            limit--;
        }
        return setAuthorName(blogList, page, (int) Math.ceil(blogs.size() / (double) LIMIT));
    }

    public BlogResult getSavedBlogId(Integer id, Authentication authentication) {
        checkAccess(id, authentication);
        return setAuthorName(blogRepository.findBySaveDraftAndAuthorId(true, id), 0, 1);
    }

}
