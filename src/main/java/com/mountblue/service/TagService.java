package com.mountblue.service;

import com.mountblue.entity.Blog;
import com.mountblue.entity.Tag;
import com.mountblue.repository.TagRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

import static com.mountblue.constant.Constant.*;
import static com.mountblue.constant.Error.*;

@Service
public class TagService {

    private final UserService userService;
    private final TagRepository tagRepository;

    public TagService(TagRepository tagRepository, UserService userService) {
        this.tagRepository = tagRepository;
        this.userService = userService;
    }

    public Set<Tag> save(String tags, Blog blog) {
        Set<String> tagList = new HashSet<>(Arrays.asList(tags.split(SPACE)));
        Set<Tag> tagSet = new HashSet<>();
        HashMap<String, Integer> hashMap = new HashMap<>();
        listAll().forEach(tag -> hashMap.put(tag.getName(), tag.getId()));
        tagList.forEach(tag -> {
            if (!tag.isEmpty()) {
                Tag tagObject;
                if (hashMap.containsKey(tag)) {
                    tagObject = get(hashMap.get(tag));
                } else {
                    tagObject = new Tag();
                    tagObject.setName(tag);
                }
                Set<Blog> blogSet = tagObject.getBlogs();
                if (blogSet == null) {
                    blogSet = new HashSet<>();
                }
                blogSet.add(blog);
                tagObject.setBlogs(blogSet);
                tagSet.add(tagObject);
                tagRepository.save(tagObject);
            }
        });
        return tagSet;
    }

    public List<Tag> listAll() {
        return tagRepository.findAll();
    }

    public Tag get(Integer id) {
        Optional<Tag> tag = tagRepository.findById(id);
        return tag.orElseGet(Tag::new);
    }

    public Set<Blog> getBlogsWithTag(String tag) {
        Set<Blog> blogSet = new HashSet<>();
        Optional<Tag> tagOb = tagRepository.findByName(tag);
        Tag tagObject = tagOb.orElseGet(Tag::new);
        try {
            tagObject.getBlogs().forEach(blog -> {
                blog.setAuthorName(userService.get(blog.getAuthorId()).getUserName());
                if (!blog.isSaveDraft()) {
                    blogSet.add(blog);
                }
            });
        } catch (NullPointerException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NO_RECORD_FOUND);
        }
        return blogSet;
    }

    public Tag getTag(String tag) {
        return tagRepository.findByName(tag).orElseGet(Tag::new);
    }

    public void delete(Set<Tag> tagSet) {
        tagSet.forEach(tag -> tagRepository.deleteById(tag.getId()));
    }


}
