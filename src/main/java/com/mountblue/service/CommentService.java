package com.mountblue.service;

import com.mountblue.entity.Comment;
import com.mountblue.entity.User;
import com.mountblue.repository.CommentRepository;
import com.mountblue.request.CommentData;
import com.mountblue.response.Status;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static com.mountblue.constant.Constant.*;
import static com.mountblue.constant.Error.*;

@Slf4j
@Service
public class CommentService {

    private final BlogService blogService;
    private final UserService userService;
    private final CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository, UserService userService, BlogService blogService) {
        this.commentRepository = commentRepository;
        this.userService = userService;
        this.blogService = blogService;
    }

    private void checkAccess(int id, Authentication authentication) {
        if (userService.getUserDetails(authentication.getName()).getId() != id &&
                !authentication.getAuthorities().contains(new SimpleGrantedAuthority(ROLE_ADMIN))) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_ACCESS);
        }
    }

    private void checkBlog(int id, Authentication authentication) {
        int authorId = userService.getId(get(id).getUserName());
        if (authorId == 0) {
            log.warn("Resolved[ Not Found : Comment not found]");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, COMMENT_NOT_FOUND);
        }
        checkAccess(authorId, authentication);
    }

    public Comment get(Integer id) {
        Optional<Comment> comment = commentRepository.findById(id);
        return comment.orElseGet(Comment::new);
    }

    public Status save(Integer id, CommentData commentData, Authentication authentication) {
        if (commentData.getComment().length() == 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, INVALID_COMMENT);
        }
        Comment commentObj = new Comment();
        commentObj.setComment(commentData.getComment());
        User user = userService.getUserDetails(authentication.getName());
        commentObj.setUserName(user.getUserName());
        commentObj.setEmail(user.getEmail());
        commentObj.setBlog(blogService.get(id));
        commentRepository.save(commentObj);
        return new Status(OK, SUCCESS);
    }

    public Status delete(Integer commentId, Authentication authentication) {
        checkBlog(commentId, authentication);
        commentRepository.deleteById(commentId);
        return new Status(OK, SUCCESS);
    }


}
