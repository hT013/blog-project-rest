package com.mountblue.service;

import com.mountblue.entity.Token;
import com.mountblue.repository.TokenRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TokenService {

    private final TokenRepository tokenRepository;

    public TokenService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public void save(Token token) {
        tokenRepository.save(token);
    }

    public Token findToken(String token) {
        return tokenRepository.findByToken(token);
    }

    public List<Token> findTokenByUser(int id) {
        List<Token> tokenList = tokenRepository.findByUser_Id(id);
        if (tokenList == null) {
            return new ArrayList<>();
        }
        return tokenList;
    }

    public void delete(Token token) {
        tokenRepository.delete(token);
    }

}
