package com.mountblue.service;

import com.mountblue.entity.Blog;
import com.mountblue.entity.Token;
import com.mountblue.entity.User;
import com.mountblue.event.SendEmailEvent;
import com.mountblue.repository.BlogRepository;
import com.mountblue.repository.UserRepository;
import com.mountblue.request.Credential;
import com.mountblue.request.Email;
import com.mountblue.request.FormInput;
import com.mountblue.request.Password;
import com.mountblue.response.JwtToken;
import com.mountblue.response.Status;
import com.mountblue.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

import static com.mountblue.constant.Constant.*;
import static com.mountblue.constant.Error.*;

@Slf4j
@Service
public class UserService {

    private final BlogRepository blogRepository;
    private final TokenService tokenService;
    private final UserRepository userRepository;
    private final RoleService roleService;
    private final JwtUtil jwtUtil;
    private final AuthenticationManager authenticationManager;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository, TokenService tokenService, RoleService roleService, ApplicationEventPublisher applicationEventPublisher, BlogRepository blogRepository, JwtUtil jwtUtil, AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.tokenService = tokenService;
        this.roleService = roleService;
        this.applicationEventPublisher = applicationEventPublisher;
        this.blogRepository = blogRepository;
        this.jwtUtil = jwtUtil;
        this.authenticationManager = authenticationManager;
        bCryptPasswordEncoder = new BCryptPasswordEncoder();
    }

    private void throwException(HttpStatus httpStatus, String message) {
        throw new ResponseStatusException(httpStatus, message);
    }

    private boolean tokenCheck(Token token) {
        boolean expired = false;
        if (new Date().getTime() - token.getExpiryDate().getTime() > 0) {
            expired = true;
        }
        return !expired;
    }

    private User checkUser(String user) {
        User userDetails = getUserDetails(user);
        if (userDetails == null) {
            log.warn("Resolved[ Invalid User : User not found]");
            throwException(HttpStatus.NOT_FOUND, USER_NOT_FOUND);
        }
        return userDetails;
    }

    private void checkEmail(Email email) {
        if (emailExist(email.getEmail().toLowerCase())) {
            throwException(HttpStatus.BAD_REQUEST, EMAIL_EXIST);
        }
        if (!Pattern.matches(EMAIL_REGEX, email.getEmail())) {
            throwException(HttpStatus.BAD_REQUEST, INVALID_DATA);
        }
    }

    private void checkError(BindingResult bindingResult, String message) {
        if (bindingResult.hasErrors()) {
            throwException(HttpStatus.BAD_REQUEST, message);
        }
    }

    private boolean emailExist(String email) {
        return userRepository.findByEmail(email) != null;
    }

    private User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    private void delete(User user) {
        userRepository.delete(user);
    }

    public User getUserDetails(String name) {
        return userRepository.findByUserName(name);
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public Boolean userExist(String name) {
        return userRepository.findByUserName(name) != null;
    }

    public User get(Integer id) {
        Optional<User> user = userRepository.findById(id);
        return user.orElseGet(User::new);
    }

    public Integer getId(String name) {
        User user = userRepository.findByUserName(name);
        if (user != null) {
            return user.getId();
        }
        return 0;
    }

    public Status savePassword(String token, Password password, BindingResult bindingResult) {
        Token tokenObj = tokenService.findToken(token);
        if (tokenObj == null || !tokenCheck(tokenObj)) {
            throwException(HttpStatus.BAD_REQUEST, INVALID_TOKEN);
        }
        checkError(bindingResult, MIN_LENGTH);
        if (!password.getPassword().equals(password.getConformPassword())) {
            throwException(HttpStatus.BAD_REQUEST, PASSWORD_SHOULD_MATCH);
        }
        User user = getUserDetails(tokenObj.getUser().getUserName());
        user.setPassword(bCryptPasswordEncoder.encode(password.getPassword()));
        save(user);
        return new Status(SUCCESS, PASSWORD_CHANGED);
    }

    public Status addUser(FormInput formInput, BindingResult bindingResult) {
        checkError(bindingResult, INVALID_DATA);
        if (userExist(formInput.getUserName())) {
            throwException(HttpStatus.BAD_REQUEST, USER_EXIST);
        }
        if (emailExist(formInput.getEmail().toLowerCase())) {
            throwException(HttpStatus.BAD_REQUEST, EMAIL_EXIST);
        }
        if (!Pattern.matches(EMAIL_REGEX, formInput.getEmail())) {
            throwException(HttpStatus.BAD_REQUEST, INVALID_DATA);
        }
        if (!formInput.getPassword().equals(formInput.getConformPassword())) {
            throwException(HttpStatus.BAD_REQUEST, PASSWORD_SHOULD_MATCH);
        }
        User user1 = new User();
        user1.setEmail(formInput.getEmail().toLowerCase());
        user1.setUserName(formInput.getUserName());
        user1.setPassword(bCryptPasswordEncoder.encode(formInput.getPassword()));
        user1.setIsEnabled(false);
        user1.setRole(roleService.getByName(ROLE_USER));
        save(user1);
        SendEmailEvent sendEmailEvent = new SendEmailEvent(this, user1,
                VERIFY_ACCOUNT_MESSAGE + VERIFY_ACCOUNT_LINK, VERIFY_ACCOUNT_SUBJECT);
        applicationEventPublisher.publishEvent(sendEmailEvent);
        return new Status(SUCCESS, LINK_SEND);
    }

    public Status conformAccount(String token) {
        Token tokenObj = tokenService.findToken(token);
        if (tokenObj != null) {
            if (tokenCheck(tokenObj)) {
                User user = getUserDetails(tokenObj.getUser().getUserName());
                user.setIsEnabled(true);
                save(user);
            }
        } else {
            throwException(HttpStatus.BAD_REQUEST, INVALID_TOKEN);
        }
        return new Status(OK, ACCOUNT_VERIFIED);
    }

    public Status sendLink(Email email, BindingResult bindingResult) {
        checkError(bindingResult, INVALID_DATA);
        User user = getUserByEmail(email.getEmail());
        if (user == null) {
            throwException(HttpStatus.BAD_REQUEST, EMAIL_DOES_NOT_EXIST);
        }
        SendEmailEvent sendEmailEvent;
        if (user.getIsEnabled()) {
            sendEmailEvent = new SendEmailEvent(this, user,
                    FORGOT_PASSWORD_MESSAGE + FORGOT_PASSWORD_LINK, FORGOT_PASSWORD_SUBJECT);
        } else {
            sendEmailEvent = new SendEmailEvent(this, user,
                    VERIFY_ACCOUNT_MESSAGE + VERIFY_ACCOUNT_LINK, VERIFY_ACCOUNT_SUBJECT);
        }
        applicationEventPublisher.publishEvent(sendEmailEvent);
        return new Status(SUCCESS, LINK_SEND);
    }

    public Status updateEmail(String user, Email email, BindingResult bindingResult) {
        checkError(bindingResult, INVALID_DATA);
        User userDetails = checkUser(user);
        userDetails.setEmail(email.getEmail().toLowerCase());
        checkEmail(email);
        save(userDetails);
        return new Status(OK, SUCCESS);
    }

    public Status updateRole(String user, String role) {
        User userObj = checkUser(user);
        userObj.setRole(roleService.getByName(role));
        save(userObj);
        return new Status(OK, SUCCESS);
    }

    public Status updatePassword(Password password, BindingResult bindingResult, Authentication authentication) {
        checkError(bindingResult, MIN_LENGTH);
        if (!password.getPassword().equals(password.getConformPassword())) {
            throwException(HttpStatus.BAD_REQUEST, PASSWORD_SHOULD_MATCH);
        }
        User user = getUserDetails(authentication.getName());
        user.setPassword(bCryptPasswordEncoder.encode(password.getPassword()));
        save(user);
        return new Status(OK, SUCCESS);
    }

    public Status changeEmail(Email email, BindingResult bindingResult, Authentication authentication) {
        checkError(bindingResult, INVALID_DATA);
        checkEmail(email);
        User user = getUserDetails(authentication.getName());
        user.setEmail(email.getEmail());
        user.setIsEnabled(false);
        save(user);
        SendEmailEvent sendEmailEvent = new SendEmailEvent(this, user,
                VERIFY_ACCOUNT_MESSAGE + VERIFY_ACCOUNT_LINK, VERIFY_ACCOUNT_SUBJECT);
        applicationEventPublisher.publishEvent(sendEmailEvent);
        return new Status(OK, LINK_SEND);
    }

    public Status deleteUser(String user) {
        User userObj = checkUser(user);
        tokenService.findTokenByUser(userObj.getId()).forEach(tokenService::delete);
        Set<Blog> blogSet = blogRepository.findByAuthorId(userObj.getId());
        blogSet.forEach(blogRepository::delete);
        delete(userObj);
        return new Status(OK, SUCCESS);
    }

    public ResponseEntity<?> generateToken(Credential credential, BindingResult bindingResult) {
        checkError(bindingResult, INVALID_DATA);
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(credential.getUserName(), credential.getPassword()));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, INVALID_CREDENTIAL);
        }
        return ResponseEntity.status(HttpStatus.OK).body(new JwtToken(SUCCESS,
                jwtUtil.generateToken(credential.getUserName())));
    }

}
