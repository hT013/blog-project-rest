package com.mountblue.api;

import com.mountblue.request.Credential;
import com.mountblue.request.Email;
import com.mountblue.request.FormInput;
import com.mountblue.request.Password;
import com.mountblue.response.Status;
import com.mountblue.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user")
    public Status addUser(@Valid @RequestBody FormInput formInput, BindingResult bindingResult) {
        return userService.addUser(formInput, bindingResult);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> generateToken(@Valid @RequestBody Credential credential, BindingResult bindingResult) {
        return userService.generateToken(credential, bindingResult);
    }

    @PutMapping("/conform-account/token/{token}")
    public Status conformAccount(@PathVariable String token) {
        return userService.conformAccount(token);
    }

    @PostMapping("/send-link")
    public Status sendLink(@Valid @RequestBody Email email, BindingResult bindingResult) {
        return userService.sendLink(email, bindingResult);
    }

    @PutMapping("/change-password/token/{token}")
    public Status savePassword(@PathVariable String token, @Valid @RequestBody Password password,
                               BindingResult bindingResult) {
        return userService.savePassword(token, password, bindingResult);
    }

}
