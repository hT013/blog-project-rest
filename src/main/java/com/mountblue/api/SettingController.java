package com.mountblue.api;

import com.mountblue.request.Email;
import com.mountblue.request.Password;
import com.mountblue.response.Status;
import com.mountblue.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.mountblue.constant.Constant.ROLE_ADMIN;
import static com.mountblue.constant.Constant.ROLE_USER;

@Slf4j
@RestController
@RequestMapping("/settings")
public class SettingController {

    private final UserService userService;

    public SettingController(UserService userService) {
        this.userService = userService;
    }

    @PutMapping("/change-email")
    public Status changeEmail(@Valid @RequestBody Email email, BindingResult bindingResult,
                              Authentication authentication) {
        return userService.changeEmail(email, bindingResult, authentication);
    }

    @PutMapping("/update-password")
    public Status updatePassword(@Valid @RequestBody Password password, BindingResult bindingResult,
                                 Authentication authentication) {
        return userService.updatePassword(password, bindingResult, authentication);
    }

    @PutMapping("/admin/make-admin/{user}")
    public Status makeAdmin(@PathVariable String user) {
        return userService.updateRole(user, ROLE_ADMIN);
    }

    @PutMapping("/admin/remove-admin/{user}")
    public Status removeAdmin(@PathVariable String user) {
        return userService.updateRole(user, ROLE_USER);
    }

    @DeleteMapping("/admin/delete-user/{user}")
    public Status deleteUser(@PathVariable String user) {
        return userService.deleteUser(user);
    }

    @PutMapping("/admin/update-email/{user}")
    public Status updateEmail(@PathVariable String user, @Valid @RequestBody Email email, BindingResult bindingResult) {
        return userService.updateEmail(user, email, bindingResult);
    }

}
