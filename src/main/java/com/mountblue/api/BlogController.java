package com.mountblue.api;

import com.mountblue.entity.Blog;
import com.mountblue.request.BlogData;
import com.mountblue.request.CommentData;
import com.mountblue.response.BlogResult;
import com.mountblue.response.Status;
import com.mountblue.service.BlogService;
import com.mountblue.service.CommentService;
import com.mountblue.service.TagService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.Set;

import static com.mountblue.constant.Constant.LIMIT;
import static com.mountblue.constant.Constant.PUBLISHED_AT;

@Slf4j
@RestController
@RequestMapping("/blogs")
public class BlogController {

    private final BlogService blogService;
    private final TagService tagService;
    private final CommentService commentService;

    public BlogController(BlogService blogService, TagService tagService, CommentService commentService) {
        this.blogService = blogService;
        this.tagService = tagService;
        this.commentService = commentService;
    }

    @GetMapping("/page/{page}")
    public BlogResult getAllBlogs(@PathVariable Integer page) {
        return blogService.getBlogsInPage(page, LIMIT, PUBLISHED_AT);
    }

    @GetMapping("/search/{value}/page/{page}")
    public BlogResult searchInBlog(@PathVariable String value, @PathVariable Integer page) {
        return blogService.getPageData(value, page, LIMIT);
    }

    @GetMapping("/fields/{field}/order/{order}/page/{page}")
    public BlogResult sortByField(@PathVariable String field, @PathVariable String order, @PathVariable Integer page) {
        return blogService.getSortedByField(field, order, page, LIMIT);
    }

    @GetMapping("/authors/{authorId}/tags/{tagId}/page/{page}")
    public BlogResult getByAuthorAndTag(@PathVariable Integer authorId, @PathVariable Integer tagId,
                                        @PathVariable Integer page) {
        return blogService.searchAuthorIdAndTag(authorId, PUBLISHED_AT, page, LIMIT, tagService.get(tagId));
    }

    @GetMapping(value = "/filter/tags/{tag}")
    public Set<Blog> filterByTag(@PathVariable String tag) {
        return tagService.getBlogsWithTag(tag);
    }

    @GetMapping("/{id}")
    public Blog showBlog(@PathVariable Integer id) {
        return blogService.getBlog(id);
    }

    @GetMapping("/authors/{id}")
    public BlogResult getAuthorPublishedBlogs(@PathVariable Integer id) {
        return blogService.getPublishedBlog(id);
    }

    @GetMapping("/auth/authors/{id}/author-saved-blog")
    public BlogResult authorSavedBlog(@PathVariable Integer id, Authentication authentication) {
        return blogService.getSavedBlogId(id, authentication);
    }

    @PostMapping("/auth/{operation}")
    public Status publishOrSaveBlog(@PathVariable String operation, @Valid @RequestBody BlogData blogData,
                                    BindingResult bindingResult, Authentication authentication) {
        return blogService.publishOrSaveBlog(operation, blogData, bindingResult, authentication);
    }

    @PutMapping("/auth/{id}")
    public Status updateBlog(@PathVariable Integer id, @Valid @RequestBody BlogData blogData,
                             BindingResult bindingResult, Authentication authentication) {
        return blogService.update(id, blogData, bindingResult, authentication);
    }

    @DeleteMapping("/auth/{id}")
    public Status deleteBlog(@PathVariable Integer id, Authentication authentication) {
        return blogService.delete(id, authentication);
    }

    @PostMapping(value = "/auth/{id}/comments")
    public Status addComment(@PathVariable int id, @RequestBody CommentData commentData, Authentication authentication) {
        return commentService.save(id, commentData, authentication);
    }

    @DeleteMapping(value = "/auth/comments/{commentId}")
    public Status deleteComment(@PathVariable int commentId, Authentication authentication) {
        return commentService.delete(commentId, authentication);
    }

}
