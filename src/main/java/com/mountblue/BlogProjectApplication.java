package com.mountblue;

import com.mountblue.configuration.SetInitialData;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableSwagger2
public class BlogProjectApplication {

    private final SetInitialData setInitialData;

    public BlogProjectApplication(SetInitialData setInitialData) {
        this.setInitialData = setInitialData;
    }

    @PostConstruct
    public void initialize() {
        setInitialData.setData();
    }

    public static void main(String[] args) {
        SpringApplication.run(BlogProjectApplication.class, args);
    }


}
