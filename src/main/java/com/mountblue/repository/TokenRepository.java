package com.mountblue.repository;

import com.mountblue.entity.Token;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TokenRepository extends JpaRepository<Token, Integer> {

    Token findByToken(String token);

    List<Token> findByUser_Id(Integer id);

}
